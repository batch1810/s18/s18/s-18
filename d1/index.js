// console.log("Hello World");

// [SECTION] Objects
// An object is a data type that is used to represent real world objects.
// It is a collection of related data and/or functionalities
// Information stored in objects are represented in a "key:value" pair.
	// key is also known as "property"
// Different data types may be stored in an object property.

/*
	Two ways in creating object in JavaScript
	1. Object Literal Notation
	2. Object Constructor Notation

*/

// Creating objects using object initializer/literal notation.

/*
	Syntax:
	let/const objectName = {
		keyA:valueA,
		keyB:valueB
	}

*/

// Cellphone is an example of real world object
let cellphone = {
	name: "Nokia 3310",
	manufactureDate: 1999
}

console.log("Result from creating objects using initializers/literal notation: ");
console.log(cellphone);

let cellphone2 = {
	name: "Iphone 13 pro max",
	manufactureDate: 2021
}

console.log(cellphone2);

let cellphone3 = {
	name: "Poco x3 gt",
	manufactureDate: 2021
}
console.log(cellphone3);

// Creating Objects using a constructor function
// Creates a reusable function to create several objects that have the same property/structure.

/*
	
	Syntax:

	function objectName(keyA, keyB){
		this.keyA = keyA;
		this.keyB = keyB;
	}

*/

function Laptop(name, manufactureDate){
	// "this" keyword allows us to assign a new object's properties by associating with the value recieved from the constructor functions's parameter.
	// console.log(this);
	this.name = name;
	this.manufactureDate = manufactureDate;
}
			// create instance of an object
let laptop = new Laptop("Dell", 2005);
console.log("Result from creating objects using object constructor: ");
console.log(laptop);

// let oldLaptop = Laptop("Portal R2E CCMC", 1980);
// console.log("Result from creating objects without the new keyword: ");

// console.log(oldLaptop);

let myLaptop = new Laptop("MacBook Air", 2020);
console.log(myLaptop);
console.log(typeof myLaptop);

// Creating empty objects
let computer = {}; //common used to create empty objects
let myComputer = new Object();

// [SECTION] Accessing Object Properties

// Using the dot notation (object.property)
console.log("Result from dot notation: " +myLaptop.name+" "+myLaptop.manufactureDate);

// Using the square bracket notation (object["property"])
console.log("Result from the square bracket notation: " +myLaptop['name']);

// Contains the instace object
let objArr = [laptop, myLaptop];

// recommended to use if the object is inside the array
console.log(objArr[0].name);

// to avoid confusion for accesing array index.
console.log(objArr[0]['name']);




// [SECTION]

let car = {};

// Initializing or adding object properties using dot notation

car.name = "Honda Civic";
console.log(car);
// Initializing/adding object properties using bracket notation

car["manufactured date"] = 2019;
console.log(car);

//delete property

delete car["manufactured date"] 
console.log(car);

// Object Methods

let person ={
	name:"Jason",
	talk: function(){
		console.log("Hi i am " + this.name);
	}
}
console.log(person);
person.talk();

// adding methods to objects

person.walk = function(steps){
	console.log(this.name + " walk" + "" + steps +" forward")
}
person.walk(50);

// let friend = {
// 	firstName: "Moira",
// 	lastName: "Dela Torre",
// 	address :{
// 		 city: "Quezon City"
// 	},
// 	emails: ["moira@mail.com", "moimoi@mail.com"],
// 	introduce: function(){
// 		console.log("Hello my name is " + this.firstName)
// 	}
// }
// friend.introduce();



let friend = {
	firstName: "Moira",
	lastName: "Dela Torre",
	address :{
		 city: "Quezon City"
	},
	emails: ["moira@mail.com", "moimoi@mail.com"],
	introduce: function(){
		console.log("Hello my name is " + this.firstName)
	}
}
friend.introduce();



let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled target Pokemon")
		console.log("targetPokemon's health is reduce to newTaretPokemonHealth")
	},
	faint: function(){
		console.log("Pokemon fainted");
	}
}

console.log(myPokemon);

// Object constructor

function Pokemon(name,level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = level *2;
	this.attack = level;

	//Methoda              //targetPokemon
	this.tackle = function(target){
		console.log(this.name + " tacked " + target.name);
		console.log("targetPokemon's health is reduce to newTaretPokemonHealth")
	    target.faint()
	}

	this.faint = function(){
		console.log(this.name + " fainted ");
	}
}

let pikachu = new Pokemon("Pikachu", 16)
console.log(pikachu);

let ratata = new Pokemon("Ratata", 8)
console.log(ratata);

pikachu.tackle(ratata)
ratata.tackle(pikachu)



