let trainer ={};
trainer.name = "Prescilla Quiban";
trainer.age = 25;
trainer.pokemon = ["Pikachu", "Ratata", "Syduck", "Bulbazaur"];
trainer.friends = {
	work: ["Chao", "Fren"],
	classmates: ["Elvs", "Rose"]
};
trainer.talk = function(){
	console.log(this.pokemon[0] + "! I choose you!");
}

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method")
trainer.talk();

function Pokemon(name,level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = level *2;
	this.attack = level;

	//Method              //targetPokemon
	this.tackle = function(target){
		console.log(this.name + " tacked " + target.name);
		target.health -= this.attack;
		if (target.health <= 0){
			target.faint();
		}
		else {
			console.log(target.name + "'s health is now reduced to " + target.health);
		}
	}

	this.faint = function(){
		console.log(this.name + " fainted ");
	}
}

let pikachu = new Pokemon("Pikachu", 16);
console.log(pikachu);

let ratata = new Pokemon("Ratata", 20);
console.log(ratata);

let syduck = new Pokemon("Syduck", 30);
console.log(syduck);

pikachu.tackle(ratata);